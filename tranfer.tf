

data "aws_iam_policy_document" "assume_sftp" {
  statement {
    sid    = "1"
    effect = "Allow"

    principals {
      identifiers = ["transfer.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "sftp"  {
  statement {
    sid    = "sftpIdpLambdaResourcePermissions"
    effect = "Allow"

    actions = [
      "execute-api:Invoke",
    ]

    resources = ["arn:aws:execute-api:eu-central-1:448965750639:kx1upyauo5/prod/GET/*"]
  }
  statement {
    sid    = "SSMLambdaResourcePermissions"
    effect = "Allow"

    actions = [
      "apigateway:GET",
    ]

    resources = ["*"]
  }
}


resource "aws_iam_policy" "sftp" {
  name_prefix = "gdl-${local.environment}-${local.application_id}-sftp-policy-01-"
  path        = "/"
  policy      = data.aws_iam_policy_document.sftp.json
}

resource "aws_iam_role" "sftp" {
  name               = "gdl-${local.environment}-${local.application_id}-sftp-role-01"
  assume_role_policy = data.aws_iam_policy_document.assume_sftp.json
}

resource "aws_iam_role_policy_attachment" "sftp" {
  role       = aws_iam_role.sftp.name
  policy_arn = aws_iam_policy.sftp.arn
}


data "aws_iam_policy_document" "logging_assume_sftp" {
  statement {
    sid    = "1"
    effect = "Allow"

    principals {
      identifiers = ["transfer.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "logging_sftp"  {
  statement {
    sid    = "sftpIdpLambdaResourcePermissions"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "logging_sftp" {
  name_prefix = "gdl-${local.environment}-${local.application_id}-logging_sftp-policy-01-"
  path        = "/"
  policy      = data.aws_iam_policy_document.logging_sftp.json
}

resource "aws_iam_role" "logging_sftp" {
  name               = "gdl-${local.environment}-${local.application_id}-logging_sftp-role-01"
  assume_role_policy = data.aws_iam_policy_document.logging_assume_sftp.json
}

resource "aws_iam_role_policy_attachment" "logging_sftp" {
  role       = aws_iam_role.logging_sftp.name
  policy_arn = aws_iam_policy.logging_sftp.arn
}

resource "aws_transfer_server" "sftp" {
  endpoint_type = "VPC_ENDPOINT"

  endpoint_details {
      vpc_endpoint_id = aws_vpc_endpoint.sftp_ep.id
  }
  logging_role = aws_iam_role.logging_sftp.arn
  protocols   = ["SFTP"]
  invocation_role = aws_iam_role.sftp.arn

  identity_provider_type = "API_GATEWAY"
  url                    = aws_api_gateway_stage.prod.invoke_url
}

resource "aws_cloudwatch_log_group" "sftp" {
  name              = "/aws/transfer/${aws_transfer_server.sftp.id}"
  retention_in_days = 1
}

resource "aws_vpc_endpoint" "sftp_ep" {
  vpc_id       = local.vpc_id
  service_name = "com.amazonaws.eu-central-1.transfer.server"
  vpc_endpoint_type = "Interface"
  security_group_ids = ["sg-052490f37a55465ff"]
  subnet_ids = local.private_subnets
  
  tags = {
    Name = "SFTP_VPC_EP"
  }
}
