

provider "aws" {
  version = "~> 3"
  region  = "eu-central-1"

}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

