

locals {
  environment = "ray"
  application_id = "sftp"
  lambda_function_name = "gdl-${local.environment}-${local.application_id}-lambda"
  vpc_id = "vpc-0d45ef1731a07392f"
  private_subnets = ["subnet-05a77ee67d8e712fc", "subnet-085f7298e0a12c63e"]
}
data "aws_iam_policy_document" "assume_idp_lambda" {
  statement {
    sid    = "1"
    effect = "Allow"

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "idp_lambda"  {
  statement {
    sid    = "sftpIdpLambdaResourcePermissions"
    effect = "Allow"

    actions = [
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets",
      "ec2:DescribeVpcs",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "lambda:InvokeFunction",
    ]

    resources = ["*"]
  }
  statement {
    sid    = "SSMLambdaResourcePermissions"
    effect = "Allow"

    actions = [
      "ssm:Describe*",
      "ssm:Get*",
      "ssm:List*"
    ]

    resources = ["*"]
  }
}

data "archive_file" "idp_lambda" {
  type        = "zip"
  source_dir  = "${path.module}/lambda/"
  output_path = "${path.module}/sftp-idp.zip"
}

resource "aws_security_group" "idp_lambda" {
  name_prefix = "gdl-${local.environment}-${local.application_id}-sftp_idp-lambda-01-"

  description = "Custom authorizer (AD) function for ${local.environment}"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_iam_policy" "idp_lambda" {
  name_prefix = "gdl-${local.environment}-${local.application_id}-policy-01-"
  path        = "/"
  policy      = data.aws_iam_policy_document.idp_lambda.json
}

resource "aws_iam_role" "idp_lambda" {
  name               = "gdl-${local.environment}-${local.application_id}-role-01"
  assume_role_policy = data.aws_iam_policy_document.assume_idp_lambda.json
}

resource "aws_iam_role_policy_attachment" "idp_lambda" {
  role       = aws_iam_role.idp_lambda.name
  policy_arn = aws_iam_policy.idp_lambda.arn
}

resource "aws_cloudwatch_log_group" "idp_lambda" {
  name              = "/aws/lambda/${local.lambda_function_name}"
  retention_in_days = 1
}

resource "aws_lambda_function" "idp_lambda" {
  filename         = "${path.module}/sftp-idp.zip"
  function_name    = local.lambda_function_name
  role             = aws_iam_role.idp_lambda.arn
  handler          = "function.lambda_handler"
  source_code_hash = data.archive_file.idp_lambda.output_base64sha256
  runtime          = "python3.8"
  memory_size      = "2048"
  timeout         = 15
  vpc_config {
    subnet_ids         = local.private_subnets
    security_group_ids = [aws_security_group.idp_lambda.id]
  }

}


