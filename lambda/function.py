import json
import logging
import boto3
from ldap3 import Server, Connection, SAFE_SYNC
import ldap3
import os
import sys
import re


logger = logging.getLogger()
logger.setLevel(logging.INFO)
LDAP_HOST = os.environ.get("LDAP_HOST") or "ldaps-gdl.eeghlan.net"
LDAP_USER_BASE = os.environ.get("LDAP_USER_BASE") or "OU=NL,DC=eeghlan,DC=net"


raw_policy = """{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowListingOfUserFolder",
            "Action": [
                "s3:ListBucket"
            ],
            "Effect": "Allow",
            "Resource": [
            ]
        },
        {
            "Sid": "AWSTransferRequirements",
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets",
                "s3:GetBucketLocation"
            ],
            "Resource": "*"
        },
        {
            "Sid": "HomeDirObjectAccess",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObjectVersion",
                "s3:DeleteObject",
                "s3:GetObjectVersion"
            ],
            "Resource": [ ]
        }
    ]
}"""

def get_ssm_groupmap():
    """ 
    Get the group definition from ssm
    Inputs:
    Outputs:
        a list of group definitions
    """
    groupmap = {}
    client = boto3.client('ssm')
    p = client.get_paginator('get_parameters_by_path')
    paginator = p.paginate(Path='/sftp', Recursive=True).build_full_result()
    for page in paginator['Parameters']:
        groupname = page['Name'].split("/")[-1]
        response = client.get_parameter(Name=page['Name'])
        value = response['Parameter']['Value']
        logger.info(f"Name is: {page['Name']}")
        logger.info(f"groupname is: {groupname}")
        logger.info(f"Value is: {value}")
        groupmap[groupname] = json.loads(value)

        a = groupmap[groupname]
        logger.info(f"type(a) is: {type(a)}")

        logger.info(f"Short is: {a['Short']}")
    logger.info(f"groupmap is: {groupmap}")
    return groupmap


def get_ldap_groups(username: str, password: str) -> list:
    """
    the user has an AD account, authenticating and getting groups from AD
    Inputs:
      username: the name of the user (ext-...)
      password: authentiation mechanism
    Outputs:
      list of groups
    """

    try:
        logger.info("going to connect to the ad")
        ldap = ldap3.Connection(
            ldap3.Server(LDAP_HOST, use_ssl=True, get_info=ldap3.ALL, connect_timeout=15),
            user=f"{username}",
            password=f"{password}",
            auto_bind=ldap3.AUTO_BIND_NO_TLS,
            read_only=True,
            check_names=True
        )

        logger.info("connected successful")
        who_am_i = ldap.extend.standard.who_am_i().split("\\")[1]
        logger.info(f"who_am_i: {who_am_i}")
        search_filter = f"(&(objectClass=user)(sAMAccountName={who_am_i}))"
        logger.debug(f"search filter = '{search_filter}'")
        ldap.search(
            search_base=LDAP_USER_BASE,
            search_filter=search_filter,
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True
        )
        logger.debug(f"Search for user {who_am_i} returned {len(ldap.entries)} entries")
        logger.debug(f"ldap_entries: {ldap.entries}")

        ldap_groups = []
        for entry in ldap.entries:
            # logger.debug(f"LDAP entry: {entry}")

            for group_dn in entry.memberOf:
                ldap_groups.append(re.split("(,|=)", group_dn)[2])
        logger.info(f"ldap_groups: {ldap_groups}")
        return ldap_groups
    except Exception as e:
        logger.info(f"ldap: {e}")
        return []


def get_cognito_group(username: str, password: str) -> list:
    """
    the user is external, authenticating and getting groups from cognito
    Username starts with ext-
    Inputs:
      username: the name of the user (ext-...)
      password: authentiation mechanism
    Outputs:
      list of groups
    """

    # this function needs to be created.
    return []


def lambda_handler(event, context):
    """
    Handler for the authentication, will connect either to ldap or cognito
    for authentication, depending on the prefix of the username
    (no prefix -> ad)
    Inputs:
        event: an object that contains the values with the function is called
        context: AWS information about the function itself.
    Output:
        a json with the policy and sftp settings
    """

    response = {}
    mapping = []

    if "username" not in event or "password" not in event:
        return response
    logger.info("User Name [{}]".format(event["username"]))

    username = event["username"]
    password = event["password"]

    groupmap = get_ssm_groupmap()
    ###
    # Authenticate and get groups
    ###
    if username.startswith("ext-"):
        logger.info("Get Cognito groups")

        groups = get_cognito_group(username, password)
    else:
        logger.info("Get AD groups")

        groups = get_ldap_groups(username, password)

    groups_intersect = list(set(groups) & set(groupmap.keys()))

    ###
    # compose return json
    ###
    policy = json.loads(raw_policy)
    logger.info(f"1. policy: {policy}")

    # homedir setting
    mapping.append({"Entry": "/" + username, "Target": "/sftp-test-mappie/" + username})
    policy['Statement'][2]['Resource'].append("arn:aws:s3:::sftp-test-mappie/" + username)
    policy['Statement'][2]['Resource'].append("arn:aws:s3:::sftp-test-mappie/" + username + "*")

    # groupdir settings
    for group in groups_intersect:
        logger.info(f"group {group}")
        mapping.append({"Entry": groupmap[group]['Short'], "Target": groupmap[group]['Path']})
        policy['Statement'][0]['Resource'].append(groupmap[group]['Bucket'])
        policy['Statement'][2]['Resource'].append(groupmap[group]['Bucket'] + groupmap[group]['BucketKey'])
        policy['Statement'][2]['Resource'].append(groupmap[group]['Bucket'] + groupmap[group]['BucketKey'] + "*")

    # glue it together
    response["Role"] = "arn:aws:iam::448965750639:role/test-ray-sftp-okta-TransferS3Access-1CKBQY5ILXTPX"
    response['HomeDirectoryType'] = "LOGICAL"
    response['HomeDirectoryDetails'] = json.dumps(mapping)
    logger.info(f"2. json.dumps(response): {json.dumps(response)}")
    response["Policy"] = json.dumps(policy)
    logger.info(f"3. json.dumps(response): {json.dumps(response)}")
    logger.info("response: {}".format(json.dumps(response)))

    return response

